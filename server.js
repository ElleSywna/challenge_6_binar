const express = require("express");
const bodyParser = require("body-parser");
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const cors = require("cors");

const app = express();
const PORT = 2000;

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
//Akses public (nge-get image)
app.use(express.static('public'));


//Import Controllers
const authController = require("./controllers/authController");
const carsController = require("./controllers/carsController");

// Import Midleware
const middleware = require("./middlewares/auth");


// Define Routes
// Auth
app.post("/auth/register", authController.register);
app.post("/auth/register/admin", middleware.authenticate, middleware.isSuperAdmin, authController.register);
app.post("/auth/login", authController.login);
app.post("/auth/login-google", authController.loginGoogle);
app.get("/auth/me", middleware.authenticate, authController.currentUser);

//CRUD cars
app.get("/cars", middleware.authenticate, middleware.roles, carsController.getAll);
app.post("/cars", middleware.authenticate, middleware.roles, carsController.create);
app.put("/cars/:id", middleware.authenticate, middleware.roles, carsController.update);
app.delete("/cars/:id", middleware.authenticate, middleware.roles, carsController.deleteCar);
app.get("/cars/filter?", carsController.filtered );

//API Documentation
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(PORT, () => {
  console.log(`Server berhasil berjalan di port http://localhost:${PORT}`);
});

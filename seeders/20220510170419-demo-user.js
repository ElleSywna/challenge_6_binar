'use strict';

const { ROLES } = require("../lib/const");
const bcrypt = require ("bcrypt");
const SALT_ROUND = 10;
const password = "superadmin";

module.exports = {
  async up (queryInterface, Sequelize) {
      const hashedPassword = await bcrypt.hash (password, SALT_ROUND);
      return queryInterface.bulkInsert('users', [{
        name: 'helen',
        email: 'superadmin@gmail.com',
        password: hashedPassword,
        role: ROLES.SUPERADMIN,
        createdAt: new Date(),
        updatedAt: new Date()
      }]);
    },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('users', null, {});
  }
};

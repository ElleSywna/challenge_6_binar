<!-- Super Admin -->
email: superadmin@gmail.com,
password: superadmin

<!-- Endpoint Swagger -->
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
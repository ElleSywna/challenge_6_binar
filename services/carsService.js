const carsRepository = require("../repositories/carsRepository");

class CarsService {
    static async create({
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        created_by,
    }) {
        try {
            if (!plate) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Plate wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!manufacture) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Manufacture wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!model) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Model wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!image) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Image wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!rent_per_day) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Rent_per_day wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!capacity) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Capacity wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!description) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Description wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!transmission) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Transmission wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!type) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Type wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!year) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Year wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!options) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Options wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!specs) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Specs wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!available_at) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Available_at wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if (!is_with_driver) {
                return {
                    status: false,
                    status_code: 400,
                    message: "is_with_driver wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            const createdCar = await carsRepository.create({
                plate,
                manufacture,
                model,
                image,
                rent_per_day,
                capacity,
                description,
                transmission,
                type,
                year,
                options,
                specs,
                available_at,
                is_with_driver,
                created_by,
            });

            return {
                status: true,
                status_code: 201,
                message: "Data mobil berhasil dibuat",
                data: {
                    created_car: createdCar,
                },
            };
        } catch (err) {
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    created_car: null,
                },
            };
        }
    }

    static async getAll() {
        try {
            const getCars = await carsRepository.getAll();

            return {
                status: true,
                status_code: 201,
                message: "Data mobil berhasil ditampilkan",
                data: {
                    get_cars: getCars,
                },
            };
        } catch (err) {
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    get_cars: null,
                },
            };
        }
    }

    static async update({
        id,
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        updated_by,
    }) {
        try {
            const updatedPost = await carsRepository.update({
                id,
                plate,
                manufacture,
                model,
                image,
                rent_per_day,
                capacity,
                description,
                transmission,
                type,
                year,
                options,
                specs,
                available_at,
                is_with_driver,
                updated_by,
            });

            return {
                status: true,
                status_code: 200,
                message: "Car updated successfully",
                data: {
                    updated_post: updatedPost,
                },
            };

        } catch (err) {
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    created_car: null,
                },
            };
        }
    }

    static async deleteCar({ id, deleted_by }) {
        try {
            const deletedCar = await carsRepository.deleteCar({
                id,
                deleted_by,
            });

            return {
                status: true,
                status_code: 200,
                message: "Car deleted successfully",
                data: {
                    deleted_car: deletedCar,
                },
            };
        } catch (err) {
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    created_car: null,
                },
            };
        }
    }


    static async filtered({ is_with_driver, available_at, capacity }) {
        try {
            const getAllCars = await carsRepository.getAllCars({
                is_with_driver,
                available_at,
                capacity,
            });

            return {
                status: true,
                code_status: 200,
                message: "Data mobil berhasil ditampilkan",
                data: {
                    filtered_cars: getAllCars,
                },
            };
        } catch (err) {
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    filtered_cars: null,
                },
            };
        }
    }


    // static async filtered({date, time, passenger}) {
    //     try {
    //         const car = await carsRepository.filter({date, time, passenger});
    //         if (car.length === 0) {
    //             return {
    //                 status: true,
    //                 code_status: 200,
    //                 message: "Data mobil berhasil ditampilkan",
    //                 data: {
    //                     filtered_cars: car,
    //                 },
    //             };
    //         }

    //     } catch (err) {
    //         return {
    //             status: false,
    //             code_status: 500,
    //             message: err.message,
    //             data: {
    //                 filtered_cars: null,
    //             },
    //         };
    //     }
    // }

}

module.exports = CarsService;

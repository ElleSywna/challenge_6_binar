const carsService = require("../services/carsService");

const create = async (req, res, next) => {
    const {
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
    } = req.body;

    const created_by = req.user.name;

    const { status, status_code, message, data } = await carsService.create({
        created_by,
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
    });

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
};

const getAll = async (req, res) => {
    const { status, status_code, message, data } = await carsService.getAll();

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
};

const update = async (req, res, next) => {
    const { id } = req.params;
    const {
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
    } = req.body;

    const updated_by = req.user.name;

    const { status, status_code, message, data } = await carsService.update({
        id,
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        updated_by,
    });

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
};

const deleteCar = async (req, res) => {
    const { id } = req.params;
    const deleted_by = req.user.name;

    const { status, status_code, message, data } = await carsService.deleteCar({
        id,
        deleted_by,
    });

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
};

const filtered = async (req, res) => {
    const { is_with_driver, available_at, capacity } = req.query;
    console.log(req.query);

    const { status, code_status, message, data } = await carsService.filtered({
        is_with_driver,
        available_at,
        capacity,
    });

    res.status(code_status).send({
        status: status,
        message: message,
        data: data,
    });
};

// const filtered = async (req, res) => {
//     const { date, time, passenger } = req.params;

//     const { status, code_status, message, data } = await carsService.filtered({
//         date, 
//         time, 
//         passenger,
//     });

//     res.status(code_status).send({
//         status: status,
//         message: message,
//         data: data,
//     });
// };


module.exports = { create, getAll, update, deleteCar, filtered };
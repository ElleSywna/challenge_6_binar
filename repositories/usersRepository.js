const { user } = require("../models");

class UsersRepository {
  static async getByEmail({ email }) {
    const getUser = await user.findOne({ where: { email} });

    return getUser;
  }

  static async create({ name, email, password, role }) {
    const createdUser = user.create({
      name,
      email,
      password,
      role,
    });

    return createdUser;
  }
}

module.exports = UsersRepository;

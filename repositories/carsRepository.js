const { car } = require("../models");
const { Op } = require("sequelize");

class CarsRepository {
    static async create({
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        created_by,
        updated_by
    }) {
        const created_cars = await car.create({
            plate,
            manufacture,
            model,
            image,
            rent_per_day,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            available_at,
            is_with_driver,
            created_by,
            updated_by
        })

        return created_cars;
    }

    static async getAll() {
        const getAll = await car.findAll();

        return getAll;
    }


    static async update({
        id,
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        updated_by,
    }) {
        const updated_cars = await car.update({
            plate,
            manufacture,
            model,
            image,
            rent_per_day,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            available_at,
            is_with_driver,
            updated_by,
        },
            { where: { id } }
        );

        return updated_cars;
    }

    static async deleteCar({ id }) {
        const deletedCar = await car.destroy({ where: { id } });

        return deletedCar;
    }

    static async getAllCars({ is_with_driver, available_at, capacity }) {
        if (is_with_driver && available_at && capacity) {
            const filteredCars = await car.findAll({
                where: {
                    is_with_driver,
                    available_at: {
                        [Op.lt]: available_at,
                    },
                    capacity,
                },
            });

            return filteredCars;
        }

        return car;
    }

    // static async filter({date, time, passenger}) {
    //     return car.findAll({
    //         where: {
    //             [Op.and]: {
    //                 available: true,
    //                 available_at: {
    //                     [Op.lte]: date + " " + time,
    //                 },
    //                 capacity: {
    //                     [Op.gte]: passenger,
    //                 },
    //             },
    //         },
    //     });
    // }

    // static async getTotalCar() {
    //     return car.count();
    // }


    // static async findAll() {
    //     return car.findAll();
    //   }


}

module.exports = CarsRepository;
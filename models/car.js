'use strict';

const { v4: uuidv4 } = require('uuid');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  car.init({
    plate: DataTypes.STRING,
    manufacture: DataTypes.STRING,
    model: DataTypes.STRING,
    image: {
      type: DataTypes.STRING,
      get() {
        const rawValue = this.getDataValue("image");
        return rawValue
          ? "http://localhost:2000" + rawValue.substring(1)
          : null;
      },
    },
    rent_per_day: DataTypes.INTEGER,
    capacity: DataTypes.INTEGER,
    description: DataTypes.STRING,
    transmission: DataTypes.STRING,
    type: DataTypes.STRING,
    year: DataTypes.STRING,
    options: DataTypes.ARRAY(DataTypes.STRING),
    specs: DataTypes.ARRAY(DataTypes.STRING),
    available_at: DataTypes.STRING,
    is_with_driver: DataTypes.BOOLEAN,
    created_by: DataTypes.STRING,
    updated_by: DataTypes.STRING,
    deleted_by: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'car',
  });
  car.beforeCreate(async (data, options) =>
  data.id = uuidv4())
  return car;
};